// Derived from neural-net-tutorial.cpp
// David Miller, http://millermattson.com/dave
// http://www.millermattson.com/dave/?p=54
// http://inkdrop.net/dave/docs/neural-net-tutorial.cpp (with *nix line endings)
// http://inkdrop.net/dave/docs/neural-net-tutorial-W.cpp (with DOS line endings)
// See the associated video for instructions: http://vimeo.com/19569529

#include "Net.hpp"
#include "TrainingData.hpp"
#include "Neuron.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>

using namespace std;

void showVectorVals(string label, vector<double> &v)
{
    cout << label << " ";
    for (unsigned i = 0; i < v.size(); ++i) {
        cout << v[i] << " ";
    }

    cout << endl;
}

void makeAllTrainingSamples(string filename)
{
    ofstream of;
    of.open(filename.c_str());

    of << "topology: 4 8 1" << endl;
    for (int i=4000; i>= 0; --i) {
        int n1 = (int)(2.0 * rand() / double(RAND_MAX));
        int n2 = (int)(2.0 * rand() / double(RAND_MAX));
        int op1 = (int)(2.0 * rand() / double(RAND_MAX));
        int op2 = (int)(2.0 * rand() / double(RAND_MAX));
        int op = op1 + op2*2;
        int t = -1;
        switch (op) {
            case 0:
                t = n1 ^ n2; // should be 0 or 1
                break;
            case 1:
                t = n1 | n2; // should be 0 or 1
                break;
            case 2:
                t = n1 & n2; // should be 0 or 1
            break;
            case 3:
                t = ~(n1 & n2); // should be 0 or 1
            break;
            default:
                t = 0;
                break;
        }
        of << "in: " << op2 << ".0 " << op1 << ".0 "  << n1 << ".0 " << n2 << ".0 " << endl;
        of << "out: " << t << ".0" << endl;
    }
}

int main()
{
    makeAllTrainingSamples("trainingData.txt");

    TrainingData trainData("trainingData.txt");

    // e.g., { 3, 2, 1 }
    vector<unsigned> topology;
    trainData.getTopology(topology);

    Net myNet(topology);

    vector<double> inputVals, targetVals, resultVals;
    int trainingPass = 0;

    while (!trainData.isEof()) {
        ++trainingPass;
        cout << endl << "Pass " << trainingPass;

        // Get new input data and feed it forward:
        if (trainData.getNextInputs(inputVals) != topology[0]) {
            break;
        }
        showVectorVals(": Inputs:", inputVals);
        myNet.feedForward(inputVals);

        // Collect the net's actual output results:
        myNet.getResults(resultVals);
        showVectorVals("Outputs:", resultVals);

        // Train the net what the outputs should have been:
        trainData.getTargetOutputs(targetVals);
        showVectorVals("Targets:", targetVals);
        assert(targetVals.size() == topology.back());

        myNet.backProp(targetVals);
        
        // Report how well the training is working, average over recent samples:
        cout << "Net recent average error: "
        << myNet.getRecentAverageError() << endl;
    }
    
    cout << endl << "Done" << endl;
}

